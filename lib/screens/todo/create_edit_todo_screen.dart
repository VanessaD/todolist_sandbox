import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/models/todo_model.dart';
import 'package:todolist_sandbox/services/firestore_database.dart';

class CreateEditTodoScreen extends StatefulWidget {
  const CreateEditTodoScreen({Key? key}) : super(key: key);

  @override
  State<CreateEditTodoScreen> createState() => _CreateEditTodoScreenState();
}

class _CreateEditTodoScreenState extends State<CreateEditTodoScreen> {
  late TextEditingController _taskController;
  late TextEditingController _extraNoteController;
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TodoModel? _todo;
  late bool _checkboxCompleted;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final TodoModel? _todoModel =
        ModalRoute.of(context)?.settings.arguments as TodoModel?;
    if (_todoModel != null) {
      _todo = _todoModel;
    }

    _taskController = TextEditingController(text: _todo?.task ?? "");
    _extraNoteController = TextEditingController(text: _todo?.extraNote ?? "");

    _checkboxCompleted = _todo?.complete ?? false;
  }

  Future<void> addTodo(TodoModel todoModel) async {
    await FirebaseFirestore.instance.collection('todos').add(todoModel.toMap());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.cancel),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(_todo != null ? "Edit Todo" : "New Todo"),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  FocusScope.of(context).unfocus();

                  final firestoreDatabase =
                  Provider.of<FirestoreDatabase>(context, listen: false);

                  firestoreDatabase.setTodo(TodoModel(
                      id: _todo?.id ?? documentIdFromCurrentDate(),
                      task: _taskController.text,
                      extraNote: _extraNoteController.text.length > 0
                          ? _extraNoteController.text
                          : "",
                      complete: _checkboxCompleted));
                  Navigator.of(context).pop();
                }
              },
              child: Text("Save"))
        ],
      ),
      body: Center(
        child: _buildForm(context),
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextFormField(
                controller: _taskController,
                style: Theme.of(context).textTheme.bodyText1,
                validator: (value) => value!.isEmpty
                    ? "todosCreateEditTaskNameValidatorMsg"
                    : null,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).iconTheme.color!, width: 2)),
                  labelText: "todosCreateEditTaskNameTxt",
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: TextFormField(
                  controller: _extraNoteController,
                  style: Theme.of(context).textTheme.bodyText1,
                  maxLines: 15,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).iconTheme.color!,
                            width: 2)),
                    labelText: "todosCreateEditNotesTxt",
                    alignLabelWithHint: true,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("todosCreateEditCompletedTxt"),
                    Checkbox(
                      value: _checkboxCompleted,
                      onChanged: (value) {
                        setState(() {
                          _checkboxCompleted = value!;
                        });
                        print('on change');
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
