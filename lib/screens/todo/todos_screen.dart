import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/models/todo_model.dart';
import 'package:todolist_sandbox/models/user_model.dart';
import 'package:todolist_sandbox/providers/auth_provider.dart';
import 'package:todolist_sandbox/services/firestore_database.dart';

import '../../app_localizations.dart';
import '../../routes.dart';
import 'empty_content.dart';

class TodosScreen extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TodosScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _appLocalizations = AppLocalizations.of(context);
    final authProvider = Provider.of<AuthProvider>(context);
    final firestoreDatabase =
        Provider.of<FirestoreDatabase>(context, listen: false);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: StreamBuilder(
            stream: authProvider.user,
            builder: (context, snapshot) {
              final UserModel? user = snapshot.data as UserModel?;
              return FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(user != null
                    ? user.email! +
                        " - " +
                        _appLocalizations.translate("homeAppBarTitle")
                    : "homeAppBarTitle"),
              );
            }),
        actions: <Widget>[
          StreamBuilder(
              stream: firestoreDatabase.todosStream(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<TodoModel> todos = snapshot.data as List<TodoModel>;
                  return Visibility(
                      visible: todos.isNotEmpty ? true : false,
                      //child: TodosExtraActions());
                      child: const Center());
                } else {
                  return const SizedBox(
                    width: 0,
                    height: 0,
                  );
                }
              }),
          IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () {
                Navigator.of(context).pushNamed(Routes.setting);
              }),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed(
            Routes.createEditTodo,
          );
        },
      ),
      body: _buildBodySection(context),
    );
  }
}

Widget _buildBodySection(BuildContext context) {
  final _appLocalizations = AppLocalizations.of(context);
  final firestoreDatabase =
      Provider.of<FirestoreDatabase>(context, listen: false);

  return StreamBuilder(
      stream: firestoreDatabase.todosStream(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return EmptyContentWidget(
            title: _appLocalizations.translate("todosErrorTopMsgTxt"),
            message: _appLocalizations
              .translate("todosErrorBottomMsgTxt"),
            key: const Key('EmptyContentWidget'),
          );
        } else if (snapshot.hasData) {
          List<TodoModel> todos = snapshot.data as List<TodoModel>;
          if (todos.isNotEmpty) {
            return ListView.separated(
              itemCount: todos.length,
              itemBuilder: (context, index) {
                return Dismissible(
                    key: Key(todos[index].id),
                    background: Container(
                      color: Colors.red,
                      child: Center(
                          child: Text(
                            _appLocalizations
                                .translate("todosDismissibleMsgTxt"),
                            style: TextStyle(color: Theme.of(context).canvasColor),
                          )),
                    ),
                    child: ListTile(
                      leading: Checkbox(
                        value: todos[index].complete,
                        onChanged: (value) {
                          TodoModel todo = TodoModel(
                              id: todos[index].id,
                              task: todos[index].task,
                              extraNote: todos[index].extraNote,
                              complete: value!);
                          firestoreDatabase.setTodo(todo);
                        },
                      ),
                      title: Text(todos[index].task),
                      onTap: () {},
                    ));
              },
              separatorBuilder: (BuildContext context, int index) {
                return const Divider(
                  height: 0.5,
                );
              },
            );
          } else {
            return EmptyContentWidget(
              title: _appLocalizations
                  .translate("todosEmptyTopMsgDefaultTxt"),
              message: _appLocalizations
                  .translate("todosEmptyBottomDefaultMsgTxt"),
              key: const Key('EmptyContentWidget'),
            );
          }
        }
        return const Center(child: CircularProgressIndicator());
      });
}
