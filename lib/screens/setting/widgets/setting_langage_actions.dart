import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/providers/language_provider.dart';

import '../../../app_localizations.dart';

enum LanguagesActions { english, french }

class SettingLanguageActions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LanguageProvider languageProvider = Provider.of(context);
    Locale _appCurrentLocale = languageProvider.appLocale;

    return PopupMenuButton<LanguagesActions>(
      icon: const Icon(Icons.language),
      onSelected: (LanguagesActions result) {
        switch (result) {
          case LanguagesActions.english:
            languageProvider.updateLanguage("en");
            break;
          case LanguagesActions.french:
            languageProvider.updateLanguage("fr");
        }
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<LanguagesActions>>[
        PopupMenuItem<LanguagesActions>(
          value: LanguagesActions.english,
          enabled: _appCurrentLocale == Locale("en") ? false : true,
          child: Text(AppLocalizations.of(context)
              .translate("settingPopUpToggleEnglish")),
        ),
        PopupMenuItem<LanguagesActions>(
          value: LanguagesActions.french,
          enabled: _appCurrentLocale == Locale("fr") ? false : true,
          child: Text(AppLocalizations.of(context)
              .translate("settingPopUpToggleFrench")),
        ),
      ],
    );
  }
}
