import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/providers/auth_provider.dart';
import 'package:todolist_sandbox/providers/theme_provider.dart';
import 'package:todolist_sandbox/screens/setting/widgets/setting_langage_actions.dart';

import '../../app_localizations.dart';
import '../../routes.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate("settingAppTitle")),
      ),
      body: _buildLayoutSection(context),
    );
  }

  Widget _buildLayoutSection(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text(
              appLocalizations.translate("settingThemeListTitle")),
          subtitle: Text(appLocalizations.translate("settingThemeListSubTitle")),
          trailing: Switch(
            activeColor: Theme.of(context).appBarTheme.backgroundColor,
            activeTrackColor: Theme.of(context).textTheme.headline1!.color,
            value: Provider.of<ThemeProvider>(context).isDarkModeOn,
            onChanged: (booleanValue) {
              Provider.of<ThemeProvider>(context, listen: false)
                  .updateTheme(booleanValue);
            },
          ),
        ),
        ListTile(
          title: Text(appLocalizations
              .translate("settingLanguageListTitle")),
          subtitle: Text(appLocalizations
              .translate("settingLanguageListSubTitle")),
          trailing: SettingLanguageActions(),
        ),
        ListTile(
          title: Text(appLocalizations.translate("settingLogoutListTitle")
              ),
          subtitle: Text(appLocalizations.translate("settingLogoutListSubTitle")),
          trailing: TextButton(
              onPressed: () {
                _confirmSignOut(context);
              },
              child: Text(appLocalizations.translate("settingLogoutButton"))),
        )

      ],
    );
  }

  _confirmSignOut(BuildContext context) {
    showPlatformDialog(
        context: context,
        builder: (_) => PlatformAlertDialog(
          material: (_, PlatformTarget target) => MaterialAlertDialogData(
              backgroundColor: Theme.of(context).appBarTheme.backgroundColor),
          title: Text(
              AppLocalizations.of(context).translate("alertDialogTitle")),
          content: Text(
              AppLocalizations.of(context).translate("alertDialogMessage")),
          actions: <Widget>[
            PlatformDialogAction(
              child: PlatformText(AppLocalizations.of(context)
                  .translate("alertDialogCancelBtn")),
              onPressed: () => Navigator.pop(context),
            ),
            PlatformDialogAction(
              child: PlatformText(AppLocalizations.of(context)
                  .translate("alertDialogYesBtn")),
              onPressed: () {
                final authProvider =
                Provider.of<AuthProvider>(context, listen: false);

                authProvider.signOut();

                Navigator.pop(context);
                Navigator.of(context).pushNamedAndRemoveUntil(
                    Routes.login, ModalRoute.withName(Routes.login));
              },
            )
          ],
        ));
  }
}
