import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/auth_widget_builder.dart';
import 'package:todolist_sandbox/constants/app_themes.dart';
import 'package:todolist_sandbox/providers/auth_provider.dart';
import 'package:todolist_sandbox/providers/language_provider.dart';

import 'package:todolist_sandbox/providers/theme_provider.dart';
import 'package:todolist_sandbox/routes.dart';
import 'package:todolist_sandbox/screens/auth/sign_in_screen.dart';
import 'package:todolist_sandbox/screens/home/home_screen.dart';
import 'package:todolist_sandbox/services/firestore_database.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'app_localizations.dart';
import 'models/user_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<ThemeProvider>(
        create: (context) => ThemeProvider(),
      ),
      ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
      ),
      ChangeNotifierProvider<LanguageProvider>(
        create: (context) => LanguageProvider(),
      ),
    ],
    child: MyApp(
      databaseBuilder: (_, uid) => FirestoreDatabase(uid: uid),
      key: const Key('MyApp'),
    ),
  ));
}

class MyApp extends StatelessWidget {
  // Expose builders for 3rd party services at the root of the widget tree
  // This is useful when mocking services while testing
  final FirestoreDatabase Function(BuildContext context, String uid)
      databaseBuilder;

  const MyApp({required Key key, required this.databaseBuilder})
      : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeProvider>(
      builder: (_, themeProviderRef, __) {
        return Consumer<LanguageProvider>(
          builder: (_, langageProviderRef, __) {
            return AuthWidgetBuilder(
                key: Key('AuthWidget'),
                databaseBuilder: databaseBuilder,
                builder:
                    (BuildContext context, AsyncSnapshot<UserModel> userSnapshot) {
                  return MaterialApp(
                    debugShowCheckedModeBanner: false,
                    locale: langageProviderRef.appLocale,
                    supportedLocales: const [
                      Locale('en', 'US'),
                      Locale('fr', 'FR'),
                    ],
                    //These delegates make sure that the localization data for the proper language is loaded
                    localizationsDelegates: const [
                      //A class which loads the translations from JSON files
                      AppLocalizations.delegate,
                      //Built-in localization of basic text for Material widgets (means those default Material widget such as alert dialog icon text)
                      GlobalMaterialLocalizations.delegate,
                      //Built-in localization for text direction LTR/RTL
                      GlobalWidgetsLocalizations.delegate,
                    ],
                    //return a locale which will be used by the app
                    localeResolutionCallback: (locale, supportedLocales) {
                      //check if the current device locale is supported or not
                      for (var supportedLocale in supportedLocales) {
                        if (supportedLocale.languageCode ==
                            locale?.languageCode ||
                            supportedLocale.countryCode == locale?.countryCode) {
                          return supportedLocale;
                        }
                      }
                      //if the locale from the mobile device is not supported yet,
                      //user the first one from the list (in our case, that will be English)
                      return supportedLocales.first;
                    },
                    title: 'Todolist Sandbox',
                    routes: Routes.routes,
                    theme: AppThemes.lightTheme,
                    themeMode: themeProviderRef.isDarkModeOn
                        ? ThemeMode.dark
                        : ThemeMode.light,
                    home: Consumer<AuthProvider>(
                      builder: (_, authProviderRef, __) {
                      if (userSnapshot.hasError) {
                          print(
                              'You have an error! ${userSnapshot.error.toString()}');
                          return const Text('Something went wrong!');
                        } else if (userSnapshot.connectionState ==
                            ConnectionState.active) {
                          return authProviderRef.status == Status.authenticated
                              ? HomeScreen()
                              : SignInScreen();
                        }
                        return const Material(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  );
                });
          }
        );
      },
    );
  }
}
