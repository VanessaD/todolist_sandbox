import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_sandbox/providers/auth_provider.dart';
import 'package:todolist_sandbox/screens/auth/sign_in_screen.dart';

class MyScaffold extends StatelessWidget {
  final Widget body;
  final FloatingActionButton? floatingActionButton;

  const MyScaffold({
    Key? key,
    required this.body,
    FloatingActionButton? this.floatingActionButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              print('logout');
              //FirebaseAuth.instance.signOut();
              final authProvider =
              Provider.of<AuthProvider>(context, listen: false);

              authProvider.signOut();
              Navigator.pop(context);
              // Navigator.of(context).pushNamedAndRemoveUntil(
              //     Routes.login, ModalRoute.withName(Routes.login));
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignInScreen()));
            },
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
