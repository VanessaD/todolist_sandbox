import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  Future<SharedPreferences>? _sharedPreference;
  static const String _isDarkMode = "is_dark_mode";
  static const String _languageCode = "language_code";

  SharedPreferenceHelper() {
    _sharedPreference = SharedPreferences.getInstance();
  }

  //Theme module
  Future<void> changeTheme(bool value) {
    return _sharedPreference!.then((prefs) {
      return prefs.setBool(_isDarkMode, value);
    });
  }

  Future<bool> get isDarkMode {
    return _sharedPreference!.then((prefs) {
      return prefs.getBool(_isDarkMode) ?? false;
    });
  }

  //Locale module
  Future<String>? get appLocale {
    return _sharedPreference?.then((prefs) {
      return prefs.getString(_languageCode) ?? 'en';
    });
  }

  // Future<void> changeLanguage(String value) async {
  //   SharedPreferences? prefs = await _sharedPreference;
  //   prefs!.setString(_languageCode, value);
  // }

  Future<void> changeLanguage(String value) {
    return _sharedPreference!.then((prefs) {
      return prefs.setString(_languageCode, value);
    });
  }
}