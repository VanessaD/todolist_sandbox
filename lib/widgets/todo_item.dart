import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:todolist_sandbox/models/todo_model.dart';

class TodoItem extends StatelessWidget {
  final TodoModel todo;
  const TodoItem({Key? key, required this.todo}) : super(key: key);

  void deleteItem(TodoModel todo) {
    //databaseReference.collection("items").doc(itemID).delete();
    FirebaseFirestore.instance
        .collection('todos')
        .doc(todo.id)
        .delete();
  }

  void toggleComplete(TodoModel todoModel) {
    FirebaseFirestore.instance
        .collection("todos")
        .doc(todoModel.id)
        .update({"complete": !todoModel.complete});
  }

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: Text(todo.task),
      value: todo.complete,
      secondary: IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.red.withOpacity(0.6),
        ),
        onPressed: () {
          deleteItem(todo);
        },
      ),
      onChanged: (bool? value) {
        toggleComplete(todo);
      },
    );
  }
}
