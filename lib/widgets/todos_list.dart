import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:todolist_sandbox/models/todo_model.dart';
import 'package:todolist_sandbox/widgets/todo_item.dart';

class TodosList extends StatelessWidget {
  final List<TodoModel> todos;
  TodosList({Key? key, required this.todos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        return ListView.builder(
          itemCount: todos.length,
          itemBuilder: (BuildContext context, index) {
            return TodoItem(todo: todos[index]);
          },
        );
  }

  void addDataToFirebase() {
    CollectionReference todos = FirebaseFirestore.instance.collection('todos');
    try {
      todos.add({
        "task": "Lire un livre pendant 30 minutes",
      }).then((value) {
        print(value.id);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}


